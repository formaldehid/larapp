export function RoutesRun($rootScope, $state, $auth) {
    'ngInject';
    
    let deregisterationCallback =  $rootScope.$on("$stateChangeStart", function(event, toState) {
        if (toState.data && toState.data.auth) {
            if (!$auth.isAuthenticated()) {
                event.preventDefault();
                return $state.go('app.login');
            }
        }
    });
    $rootScope.$on('$destroy', deregisterationCallback)
}
