export function RoutesConfig($stateProvider, $urlRouterProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`;
	};

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('app', {
			abstract: true,
            data: {},
			views: {
				header: {
					templateUrl: getView('header'),
                    controller: ($scope, $auth) => {
                        $scope.isAuthenticated = $auth.isAuthenticated();
                    }
				},
				footer: {
					templateUrl: getView('footer')
				},
				main: {}
			}
		})
		.state('app.landing', {
            data: {
                auth: true
            },
            url: '/',
            views: {
                'main@': {
                    templateUrl: getView('landing')
                }
            }
        })
        .state('app.login', {
			url: '/login',
			views: {
				'main@': {
					templateUrl: getView('login')
				}
			}
		})
        .state('app.register', {
            url: '/register',
            views: {
                'main@': {
                    templateUrl: getView('register')
                }
            }
        })
        .state('app.forgot_password', {
            url: '/forgot-password',
            views: {
                'main@': {
                    templateUrl: getView('forgot-password')
                }
            }
        })
        .state('app.reset_password', {
            url: '/reset-password/:email/:token',
            views: {
                'main@': {
                    templateUrl: getView('reset-password')
                }
            }
        })
        .state('app.logout', {
            url: '/logout',
            onEnter: ($auth, $location, ToastService, $window) => {
                if (!$auth.isAuthenticated()) { return; }
                $auth.logout()
                    .then(function() {
                        ToastService.show('You have been logged out');
                        $location.path('/');
                        $window.location.reload();
                    });
            }
        });
}
