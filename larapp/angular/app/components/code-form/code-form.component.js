class CodeFormController {
    constructor(API, ToastService, $state) {
        'ngInject';

        this.API = API;
        this.$state = $state;
        this.ToastService = ToastService;
    }

    $onInit(){
        this.code = '';
        this.submittedCode = '';
        this.API.all('code/get').post().then((response) => {
            this.submittedCode = response.data.row.code;
        });
    }

    submit() {
        this.API.all('code/submit').post({
            code: this.code
        }).then(() => {
            this.ToastService.show('Code was saved to database');
            this.submittedCode = this.code;
            this.code = '';
        }, (response) => {
            this.code = response.data.html;
        });
    }
}

export const CodeFormComponent = {
    templateUrl: './views/app/components/code-form/code-form.component.html',
    controller: CodeFormController,
    controllerAs: 'vm',
    bindings: {}
}