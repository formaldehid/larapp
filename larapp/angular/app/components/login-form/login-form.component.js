class LoginFormController {
	constructor($auth, ToastService, $location, $window) {
		'ngInject';

		this.$auth = $auth;
		this.ToastService = ToastService;
		this.$location = $location;
		this.$window = $window;
	}

    $onInit(){
        this.email = '';
        this.password = '';
    }

	login() {
		let user = {
			email: this.email,
			password: this.password
		};

		this.$auth.login(user)
			.then((response) => {
				this.$auth.setToken(response.data);
				this.ToastService.show('Logged in successfully.');
				this.$location.path('/');
				this.$window.location.reload();
			})
			.catch(this.failedLogin.bind(this));
	}

	failedLogin(response) {
		if (response.status === 422) {
			for (let error in response.data.errors) {
				return this.ToastService.error(response.data.errors[error][0]);
			}
		}
		this.ToastService.error(response.data.message);
	}
}

export const LoginFormComponent = {
	templateUrl: './views/app/components/login-form/login-form.component.html',
	controller: LoginFormController,
	controllerAs: 'vm',
	bindings: {}
}
