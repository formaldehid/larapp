import {APIService} from './services/API.service';
import {ToastService} from './services/toast.service';

angular.module('app.services')
	.service('API', APIService)
	.service('ToastService', ToastService);
