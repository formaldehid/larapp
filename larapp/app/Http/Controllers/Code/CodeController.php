<?php
namespace App\Http\Controllers\Code;

use Illuminate\Support\Facades\Validator;
use App\Code;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CodeController extends Controller
{
    public function submit(Request $request)
    {
        $this->validate($request, [
            "code" => "required"
        ]);
        
        $v = Validator::make($request->all(), [
            'code'    => 'not_script'
        ]);

        if ($v->fails()) 
        {
            $html = preg_replace('#<script[^>]*>(.*?)<\/script[^>]*>|<javascript[^>]*>(.*?)<\/javascript[^>]*>#is', '', $request->code);
            $json = [
                "message" => "The given data contains script tag/-s. Re-submit suggested code.",
                "code" => 406,
                "html" => $html
            ];
            return response(json_encode($json), 406);
        }

        $code = new Code;
        $code->code = trim($request->code);
        $code->save();

        return response()->success(true);
    }

    public function get(Request $request)
    {
        $row = Code::orderBy('created_at', 'desc')->first();

        return response()->success(compact("row"));
    }
}