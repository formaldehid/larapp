<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function() {

    Route::get('/', 'IndexController@index');

});


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

    //public API routes
    $api->group(['middleware' => ['api']], function ($api) {

        // Authentication Routes...
        $api->post('auth/login', 'App\Http\Controllers\Auth\AuthController@login');
        $api->post('auth/register', 'App\Http\Controllers\Auth\AuthController@register');

        // Password Reset Routes...
        $api->post('auth/password/email', 'App\Http\Controllers\Auth\PasswordResetController@sendResetLinkEmail');
        $api->get('auth/password/verify', 'App\Http\Controllers\Auth\PasswordResetController@verify');
        $api->post('auth/password/reset', 'App\Http\Controllers\Auth\PasswordResetController@reset');

    });

    //protected API routes with JWT (must be logged in)
    $api->group(['middleware' => ['api', 'api.auth']], function ($api) {
        
        // Submitting code
        $api->post('code/submit', 'App\Http\Controllers\Code\CodeController@submit');
        
        // Fetch Last code
        $api->post('code/get', 'App\Http\Controllers\Code\CodeController@get');
    });

});

