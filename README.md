# README #

This README would document whatever steps are necessary to get Larapp application up and running. 


More readme comming soon ...

### Summary ###

This application was made for testing Laravel and Angular.


### Features ###

* Login
* Register
* Forget / Reset password
* Submitting HTML code

### Requirements ###

* Vagrant
* Homestead
* Composer
* Npm
* Bower
* Gulp


### Need some help? ###

If you have any troubles installing or configuring Larapp, please feel free to contact me on email / skype [marko.sabec@it-portal.si](mailto:marko.sabec@it-portal.si)